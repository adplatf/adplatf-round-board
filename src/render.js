import * as PIXI from 'pixi.js'
import ifEmoji from 'if-emoji'
import {Charm} from "pixijs-charm";
import {createHover, hideHover, showHover} from "./hover";
import {teams, services, getTeamsNum, ctfTitle} from "./config";
import {getIconSprite, getNameSprite, getPointsSprite, setIconSprite, setNameSprite, setPointsSprite} from "./sprites";
import {expMap, getBezierControlPoint, quadraticBezierTweenUpdate, roundToNPlaces} from "./utils";
import {currentLeader, getTeamScores, getTeamStatuses} from "./state";

let testLines = false;
let testFlag = false;

const teamNameStyle = {fontFamily: '"Noto Sans", sans-serif', fontSize: 20, fill: 0xffffff, align: 'center'};
const leaderTeamNameStyle = {fontFamily: '"Noto Sans", sans-serif', fontSize: 20, fill: 0xffd700, align: 'center'};
const teamPointsStyle = {fontFamily: '"Noto Sans", sans-serif', fontSize: 14, fill: 0xffffff, align: 'center'};

const emoji13Supported = ifEmoji('🪙');

let app;
let container;
let flagTexture;
let charm;
let graphics;
let ovalCenterX;
let ovalCenterY;

export function initRender() {
    app = new PIXI.Application({
        width: 800, height: 600, backgroundColor: 0x141414, resolution: window.devicePixelRatio || 1,
    });

    // костыли для перехода Charm на Pixi v5
    Object.defineProperty(PIXI, 'particles', {
        value: {ParticleContainer: PIXI.ParticleContainer}
    });

    app.renderer.view.style.position = 'absolute';
    app.renderer.view.style.display = 'block';
    app.renderer.autoDensity = true;
    app.renderer.resize(window.innerWidth, window.innerHeight);

    charm = new Charm(PIXI);

    document.body.appendChild(app.view);

    container = new PIXI.Container();
    app.stage.addChild(container);

    console.log('Loading textures, creating team sprites');
    flagTexture = PIXI.Texture.from('/resources/flag.png');

    let hoverContainer = createHover();

    app.stage.addChild(hoverContainer);

    for (let i = 0; i < getTeamsNum(); i++) {
        const teamTexture = PIXI.Texture.from('/resources/logo/' + teams[i].icon);

        let overHandler = function (data) {
            console.log('Over ' + i, data);
            showHover(i);
        }

        let outHandler = function (data) {
            console.log('Out ' + i, data);
            hideHover();
        }

        const icon = new PIXI.Sprite(teamTexture);
        icon.anchor.set(0.5);
        container.addChild(icon);
        icon.width = 60;
        icon.height = 60;
        icon.interactive = true;
        icon.mouseover = overHandler;
        icon.mouseout = outHandler;
        setIconSprite(i, icon);

        const name = new PIXI.Text('Team name');
        name.anchor.set(0.5);
        name.interactive = true;
        name.mouseover = overHandler;
        name.mouseout = outHandler;
        container.addChild(name);
        setNameSprite(i, name);
        updateTeamName(i);

        const points = new PIXI.Text(getTeamScoreString(i), teamPointsStyle);
        points.anchor.set(0.5);
        points.mouseover = overHandler;
        points.mouseout = outHandler;
        container.addChild(points);
        setPointsSprite(i, points);
    }

    graphics = new PIXI.Graphics();
    container.addChildAt(graphics, 0)

    ovalCenterX = window.innerWidth / 2;
    ovalCenterY = window.innerHeight / 2;

    window.onresize = function () {
        app.renderer.resize(window.innerWidth, window.innerHeight);
    }

    document.title = ctfTitle;

    console.log('Render init complete');
}

function render() {
    const renderBoxBorderX = app.renderer.width * 0.04;
    const renderBoxBorderY = app.renderer.height * 0.05;

    ovalCenterX = window.innerWidth / 2;
    ovalCenterY = window.innerHeight / 2;
    const ovalRadiusX = ovalCenterX - renderBoxBorderX;
    const ovalRadiusY = ovalCenterY - renderBoxBorderY;

    let teamsNum = getTeamsNum();

    function normalizePosition(offset, coord, radius, center, aspectRatio) {
        let borderDist = radius - Math.abs(offset);
        let lambda = 2 * aspectRatio;
        let normalizeCoef = expMap(borderDist / radius, lambda) / lambda;
        return coord * (1 - normalizeCoef) + center * normalizeCoef;
    }

    for (let i = 0; i < teamsNum; i++) {
        let rad = 3 * Math.PI / 2 + 2 * Math.PI / teamsNum * i;

        let offsetX = Math.cos(rad) * ovalRadiusX;
        let offsetY = Math.sin(rad) * ovalRadiusY;

        let teamX = ovalCenterX + offsetX;
        let teamY = ovalCenterY + offsetY;

        if (Math.abs(window.innerWidth - window.innerHeight) > Math.min(window.innerWidth, window.innerHeight) * 0.1) {
            if (window.innerWidth > window.innerHeight) {
                teamX = normalizePosition(offsetX, teamX, ovalRadiusX, ovalCenterX,
                    window.innerWidth / window.innerHeight);
            } else if (window.innerWidth < window.innerHeight) {
                teamY = normalizePosition(offsetY, teamY, ovalRadiusY, ovalCenterY,
                    window.innerHeight / window.innerWidth);
            }
        }

        const team = getIconSprite(i);
        team.x = teamX;
        team.y = teamY;

        const name = getNameSprite(i);
        const points = getPointsSprite(i);
        name.x = team.x;
        points.x = team.x

        const epsilon = 1;
        if (team.y > window.innerHeight / 2 + epsilon) {
            name.y = team.y - 45;  // сверху
            points.y = team.y - 65;
        } else {
            name.y = team.y + 45;  // снизу
            points.y = team.y + 65;
        }
    }

    charm.update();

    if (testLines) {
        // проверка соединения команд
        graphics.clear();
        for (let i = 0; i < teamsNum; i++) {
            for (let j = 0; j < teamsNum; j++) {
                let team1 = getIconSprite(i);
                let team2 = getIconSprite(j);

                let cpX, cpY;
                [cpX, cpY] = getBezierControlPoint(team1, team2, ovalCenterX, ovalCenterY)

                graphics.lineStyle(3, 0xffffff, 1.0);
                graphics.moveTo(team1.x, team1.y);
                graphics.quadraticCurveTo(cpX, cpY, team2.x, team2.y);
            }
        }
    }

    if (testFlag) {
        console.log('Launching test flag');

        let teamNum1 = Math.floor(Math.random() * teamsNum);
        let teamNum2 = teamNum1;
        while (teamNum2 === teamNum1) {
            teamNum2 = Math.floor(Math.random() * teamsNum);
        }

        launchFlag(teamNum1, teamNum2);
        testFlag = false;
    }
}

export function updateTeamName(team) {
    let sprite = getNameSprite(team);
    let name = teams[team].name;

    if (team === currentLeader) {
        sprite.text = '👑 ' + name;
        sprite.style = leaderTeamNameStyle;
    } else {
        sprite.text = name;
        sprite.style = teamNameStyle;
    }
}

export function launchFlag(teamNum1, teamNum2) {
    console.log('Launching flag from ' + teams[teamNum1].name + ' to ' + teams[teamNum2].name);

    if (document.hidden) {
        console.log('Tab is hidden, ignoring flag');
        return;
    }

    const team1 = getIconSprite(teamNum1);
    const team2 = getIconSprite(teamNum2);

    const flag = new PIXI.Sprite(flagTexture);
    flag.anchor.set(0.5);
    flag.x = team1.x;
    flag.y = team1.y;
    flag.width = 60;
    flag.height = 60;
    container.addChildAt(flag, 0);

    let curve = [
        [team1.x, team1.y],
        getBezierControlPoint(team1, team2, ovalCenterX, ovalCenterY),
        [team2.x, team2.y]
    ]

    let tween = charm.followCurve(flag, curve, 80, 'smoothstep');
    tween.onCompleted = () => container.removeChild(flag);
    tween.update = quadraticBezierTweenUpdate(flag)
}

export function updateFrame() {
    render();
    requestAnimationFrame(updateFrame)
}

export function updateScoreText(team) {
    getPointsSprite(team).text = getTeamScoreString(team);
}

function getTeamScoreString(team) {
    const teamScores = getTeamScores(team);
    const teamServices = getTeamStatuses(team);
    let totalScore = 0;
    let totalStolenFlags = 0;
    let totalLostFlags = 0;

    for (let service = 0; service < services.length; service++) {
        totalScore += teamScores[service].score * teamServices[service].sla;
        totalStolenFlags += teamScores[service].stolenFlags;
        totalLostFlags += teamScores[service].lostFlags;
    }

    let pointsSign = emoji13Supported ? '🪙 ' : '💰 ';
    return (pointsSign + roundToNPlaces(totalScore, 2)) + ', 🚩 +' + totalStolenFlags + '/-' + totalLostFlags;
}
