let teamsSprites = [];
let teamsNames = [];
let teamsPoints = [];

export function getIconSprite(team) {
    return teamsSprites[team];
}

export function getNameSprite(team) {
    return teamsNames[team];
}

export function getPointsSprite(team) {
    return teamsPoints[team];
}

export function setIconSprite(team, sprite) {
    teamsSprites[team] = sprite;
}

export function setNameSprite(team, sprite) {
    teamsNames[team] = sprite;
}

export function setPointsSprite(team, sprite) {
    teamsPoints[team] = sprite;
}