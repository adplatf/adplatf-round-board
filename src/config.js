export let ctfTitle;
export let gameStartTime;
export let totalRounds;
export let roundLength;

export let services;
export let teams;

export function getTeamsNum() {
    return teams.length;
}

export function initConfig(data) {
    ctfTitle = data.config.ctfTitle;
    gameStartTime = new Date(data.config.gameStartTime);
    totalRounds = data.config.totalRounds;
    roundLength = data.config.roundLength;
    services = data.config.services;
    teams = data.config.teams;
}