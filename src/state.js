import {roundLength, services, teams} from "./config";
import {showFreezeText, updateRoundText} from "./overlay";
import {updateTeamName} from "./render";

export let gameStarted;
export let gameEnded;
export let currentRound;
export let freeze;
export let currentRoundStart;
export let currentRoundEnd;

export let currentLeader;

let scores = {};
let statuses = {};

export function initState(data) {
    gameStarted = data.state.gameStarted;
    gameEnded = data.state.gameEnded;
    currentRound = data.state.currentRound;
    freeze = data.state.freeze;
    currentRoundStart = new Date(data.state.currentRoundStart);
    currentRoundEnd = new Date(currentRoundStart.getTime() + roundLength * 1000);

    scores = data.scores;
    statuses = data.statuses;

    currentLeader = calculateInitialLeader();

    if (freeze) {
        showFreezeText();
    }
}

export function startNewRound(num, start) {
    currentRound = num;
    currentRoundStart = start;
    currentRoundEnd = new Date(start.getTime() + roundLength * 1000);
}

export function startGame() {
    gameStarted = true;
    updateRoundText();
}

export function endGame() {
    gameEnded = true;
}

export function startFreeze() {
    freeze = true;
    showFreezeText();
}

export function getScore(team, service) {
    return scores[team][service];
}

export function getTeamScores(team) {
    return scores[team];
}

export function getStatus(team, service) {
    return statuses[team][service];
}

export function getTeamStatuses(team) {
    return statuses[team];
}

function getTotalScore(team) {
    const teamScores = getTeamScores(team);
    const teamServices = getTeamStatuses(team);
    let totalScore = 0;

    for (let service = 0; service < services.length; service++) {
        totalScore += teamScores[service].score * teamServices[service].sla;
    }

    return totalScore
}

export function checkNewLeader(newTeam) {
    if (currentLeader !== newTeam) {
        let newTotalScore = getTotalScore(newTeam);

        if (currentLeader !== -1) {
            let leaderTotalScore = getTotalScore(currentLeader)

            if (newTotalScore > leaderTotalScore) {
                let oldLeader = currentLeader;
                currentLeader = newTeam;

                updateTeamName(oldLeader);
                updateTeamName(newTeam);
            }
        } else {
            currentLeader = newTeam;
        }
    }
}

function calculateInitialLeader() {
    let maxScore = -1;
    let maxScoreTeam;
    let maxScoreTeamsNumber = 1;

    for (let team = 0; team < teams.length; team++) {
        let score = getTotalScore(team);
        if (score > maxScore) {
            maxScore = score;
            maxScoreTeam = team;
            maxScoreTeamsNumber = 1;
        } else if (score === maxScore) {
            maxScoreTeamsNumber++;
        }
    }

    if (maxScoreTeamsNumber === 1) {
        return maxScoreTeam;
    } else {
        return -1;
    }
}
