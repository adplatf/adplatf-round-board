import * as PIXI from 'pixi.js'
import {services, teams} from "./config";
import {roundToNPlaces} from "./utils";
import {getScore, getStatus} from "./state";
import {getIconSprite} from "./sprites";

const hoverTitleStyle = {fontFamily: '"Noto Sans", sans-serif', fontSize: 18, fill: 0x000000, align: 'center'};
const hoverServiceStyle = {fontFamily: '"Noto Sans", sans-serif', fontSize: 14, fill: 0x000000, align: 'center'};
const hoverStatusStyle = {fontFamily: '"Noto Sans", sans-serif', fontSize: 14, fill: 0xff0000, align: 'center'};
const hoverLineStyle = {fontFamily: '"Noto Sans", sans-serif', fontSize: 12, fill: 0x000000, align: 'center'};

let hoverContainer;
let hoverTitle;

let hoverSlas = {};
let hoverFlags = {};
let hoverScores = {};
let hoverStatuses = {};

export function createHover() {
    hoverContainer = new PIXI.Container();
    hoverContainer.width = 150;
    hoverContainer.visible = false;

    let hoverBox = PIXI.Sprite.from(PIXI.Texture.WHITE);
    hoverBox.x = 0;
    hoverBox.y = 0;
    hoverBox.width = 150;
    hoverContainer.addChild(hoverBox);

    hoverTitle = new PIXI.Text("Team name", hoverTitleStyle);
    hoverTitle.anchor.set(0.5);
    hoverTitle.x = 75;
    hoverTitle.y = 20;
    hoverContainer.addChild(hoverTitle);

    let offset = 50;
    for (let service = 0; service < services.length; service++) {
        const hoverService = new PIXI.Text(services[service], hoverServiceStyle);
        hoverService.anchor.set(0.5);
        hoverService.x = 75;
        hoverService.y = offset;
        hoverContainer.addChild(hoverService);
        offset += 20;

        const hoverStatus = new PIXI.Text('DOWN', hoverStatusStyle);
        hoverStatus.anchor.set(0.5);
        hoverStatus.x = 75;
        hoverStatus.y = offset;
        hoverContainer.addChild(hoverStatus);
        hoverStatuses[service] = hoverStatus;
        offset += 20;

        const hoverSla = new PIXI.Text('SLA: 100%', hoverLineStyle);
        hoverSla.anchor.set(0.5);
        hoverSla.x = 75;
        hoverSla.y = offset;
        hoverContainer.addChild(hoverSla);
        hoverSlas[service] = hoverSla;
        offset += 20;

        const hoverFlag = new PIXI.Text('Flags: +0/-0', hoverLineStyle);
        hoverFlag.anchor.set(0.5);
        hoverFlag.x = 75;
        hoverFlag.y = offset;
        hoverContainer.addChild(hoverFlag);
        hoverFlags[service] = hoverFlag;
        offset += 20;

        const hoverScore = new PIXI.Text('Score: 0', hoverLineStyle);
        hoverScore.anchor.set(0.5);
        hoverScore.x = 75;
        hoverScore.y = offset;
        hoverContainer.addChild(hoverScore);
        hoverScores[service] = hoverScore;

        offset += 30;
    }

    hoverContainer.height = offset;
    hoverBox.height = offset;
    return hoverContainer;
}

export function showHover(team) {
    let sprite = getIconSprite(team);

    hoverTitle.text = teams[team].name;
    hoverTitle.style.fontSize = 18;
    while (hoverTitle.width > 150) {
        hoverTitle.style.fontSize -= 2;
    }

    if (sprite.x < window.innerWidth / 2) {
        hoverContainer.x = sprite.x;
    } else {
        hoverContainer.x = sprite.x - hoverContainer.width;
    }

    if (sprite.y < window.innerHeight / 2) {
        hoverContainer.y = sprite.y;
    } else {
        hoverContainer.y = sprite.y - hoverContainer.height;
    }

    for (let service = 0; service < services.length; service++) {
        let status = getStatus(team, service);
        let score = getScore(team, service);

        hoverSlas[service].text = 'SLA: ' + (roundToNPlaces(status.sla * 100, 2)) + '%';
        hoverFlags[service].text = 'Flags: +' + score.stolenFlags + '/-' + score.lostFlags;
        hoverScores[service].text = 'Score: ' + roundToNPlaces(score.score, 2);
        hoverStatuses[service].text = status.status;
        hoverStatuses[service].style.fill = getColorForStatus(status.status);
    }

    hoverContainer.visible = true;
}

export function hideHover() {
    hoverContainer.visible = false;
}

function getColorForStatus(status) {
    switch (status) {
        case 'UP':
            return 0x00ff00;
        case 'CORRUPT':
            return 0x0000ff;
        case 'MUMBLE':
            return 0xffa500;
        default:
            return 0xff0000;
    }
}
