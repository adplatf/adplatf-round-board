'use strict';

import {stateUrl} from "./settings";
import {initState} from "./state";
import {initRender, updateFrame} from "./render";
import {startRoundTimer, updateRoundText} from "./overlay";
import {connectToWs} from "./ws";
import {initConfig} from "./config";

window.onload = function () {
    console.log('Loading state');
    const axios = require('axios').default;

    axios.get(stateUrl)
        .then(response => {
            let data = response.data;
            console.log('Current state: ', data);
            loadApplication(data);
        })
        .catch(error => {
            console.error('Error in application: ', error);
        });

    function loadApplication(data) {
        console.log('Loading application');

        initConfig(data)
        initState(data);
        initRender();

        updateRoundText();
        startRoundTimer();

        updateFrame();
        connectToWs();

        console.log('Done initializing');
    }
}