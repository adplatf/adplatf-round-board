export function quadraticBezierTweenUpdate(displayObject) {
    return function () {
        let normalizedTime, curvedTime, p = this.pointsArray;
        if (this.playing) {
            if (this.frameCounter < this.totalFrames) {
                normalizedTime = this.frameCounter / this.totalFrames;
                curvedTime = smoothstep(normalizedTime, this.startMagnitude, 0, 1, this.endMagnitude);

                displayObject.x = quadraticBezier(curvedTime, p[0][0], p[1][0], p[2][0]);
                displayObject.y = quadraticBezier(curvedTime, p[0][1], p[1][1], p[2][1]);

                this.frameCounter += 1;
            } else {
                this.end();
            }
        }
    }
}

function smoothstep(x) {
    return x * x * (3 - 2 * x);
}

function quadraticBezier(t, a, b, c) {
    let m = 1 - t
    return m * m * a + 2 * m * t * b + t * t * c;
}

export function getBezierControlPoint(team1, team2, ovalCenterX, ovalCenterY) {
    let directLineCenterX = (team1.x + team2.x) / 2;
    let directLineCenterY = (team1.y + team2.y) / 2;
    let cpX = directLineCenterX * 0.7 + ovalCenterX * 0.3;
    let cpY = directLineCenterY * 0.7 + ovalCenterY * 0.3;

    let dX = (Math.abs(ovalCenterX - cpX) - ovalCenterX) * 0.2;
    let dY = (Math.abs(ovalCenterY - cpY) - ovalCenterY) * 0.2;

    dX = dX * 2 * Math.random() - dX;
    dY = dY * 2 * Math.random() - dY;
    cpX += dX;
    cpY += dY;

    return [cpX, cpY];
}

export function roundToNPlaces(value, n) {
    const c = Math.pow(10, n)
    return Math.round((value + Number.EPSILON) * c) / c;
}

export function expMap(x, lambda) {
    return Math.log(1 - x + Math.exp(lambda) * x) / lambda;
}