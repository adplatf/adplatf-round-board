import {wsUrl} from "./settings";
import {
    checkNewLeader,
    endGame,
    getScore,
    getStatus,
    startFreeze,
    startGame,
    startNewRound
} from "./state";
import {launchFlag, updateScoreText} from "./render";
import {showFirstBlood, updateRoundText} from "./overlay";
import {services, teams} from "./config";

export function connectToWs() {
    console.log('Connecting to WS')
    let ws = new WebSocket('ws://' + location.host + wsUrl);

    ws.onopen = function () {
        console.log('Connected to WS');
    }

    ws.onclose = function (event) {
        console.log('WS closed: ' + event.code + ' ' + event.reason);
    }

    ws.onmessage = function (event) {
        let data = event.data;
        let json = JSON.parse(data);

        console.log('Incoming message', json);
        processMessage(json)
    }

    ws.onerror = function (error) {
        console.error('WS error: ' + error.message);
    }
}

function processMessage(message) {
    const type = message.type;
    const value = message.value;

    switch (type) {
        case 'STEAL_FLAG':
            processStealFlag(value);
            break;
        case 'UPDATE_STATUS':
            processUpdateStatus(value);
            break;
        case 'UPDATE_SCORE':
            processUpdateScore(value);
            break;
        case 'GAME_START':
            processGameStart();
            break;
        case 'GAME_END':
            processGameEnd();
            break;
        case 'NEW_ROUND':
            processNewRound(value);
            break;
        case 'FREEZE_START':
            processFreezeStart();
            break;
        case 'FIRST_BLOOD':
            processFirstBlood(value);
            break;
        case 'FORCE_UPDATE_STATE':
            processForceUpdate();
            break;
        default:
            console.warn('Unknown type ' + type);
            break;
    }
}

function processStealFlag(value) {
    const {thiefTeam, victimTeam, service} = value;

    let thiefScore = getScore(thiefTeam, service);
    let victimScore = getScore(victimTeam, service);
    thiefScore.stolenFlags += 1;
    victimScore.lostFlags += 1;

    launchFlag(victimTeam, thiefTeam);
    updateScoreText(thiefTeam);
    updateScoreText(victimTeam);
}

function processUpdateStatus(value) {
    const {team, service, status, description, sla} = value;

    let teamStatus = getStatus(team, service);
    teamStatus.status = status;
    teamStatus.description = description;
    teamStatus.sla = sla;

    updateScoreText(team);
    checkNewLeader(team);
}

function processUpdateScore(value) {
    const {team, service, newScore} = value;

    let teamScore = getScore(team, service);
    teamScore.score = newScore;

    updateScoreText(team);
    checkNewLeader(team);
}

function processGameStart() {
    startGame();
}

function processGameEnd() {
    endGame();
    updateRoundText();
}

function processNewRound(value) {
    startNewRound(value.roundNum, new Date(value.roundStart))
    updateRoundText();
}

function processFreezeStart() {
    startFreeze();
}

function processFirstBlood(value) {
    let team = value.team;
    let service = value.service;
    showFirstBlood(teams[team].name, services[service]);
}

function processForceUpdate() {
    location.reload();
}
