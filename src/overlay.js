import {currentRound, currentRoundEnd, gameEnded, gameStarted} from "./state";
import {gameStartTime, totalRounds} from "./config";

let messageShown = false;

export function updateRoundText() {
    document.getElementById('round-num-box').innerText = getRoundText();
}

export function showFreezeText() {
    document.getElementById('freeze-box').style.display = 'block';
}

export function startRoundTimer() {
    setInterval(function () {
        let timeBox = document.getElementById('round-time-box');

        timeBox.innerText = getTimerText();
    }, 1000);
}

export function showFirstBlood(teamName, serviceName) {
    if (messageShown) {
        return;
    }

    let overlay = document.getElementById('overlay');
    let bloodOverlay = document.getElementById('first-blood-overlay');
    let teamBox = document.getElementById('first-blood-team');
    let serviceBox = document.getElementById('first-blood-service');

    overlay.style.opacity = '1';
    bloodOverlay.style.display = 'block';

    teamBox.innerText = 'By ' + teamName;
    teamBox.setAttribute('data-text', 'By ' + teamName);

    serviceBox.innerText = 'On ' + serviceName;
    serviceBox.setAttribute('data-text', 'On ' + serviceName);

    setTimeout(function () {
        overlay.style.opacity = '0';

        setTimeout(function () {
            bloodOverlay.style.display = 'none';
            messageShown = false;
        }, 500);
    }, 6000);

    messageShown = true;
}

function getRoundText() {
    if (!gameStarted) {
        return 'Game starts soon'
    } else if (gameEnded) {
        return 'Game over'
    } else {
        return 'Round #' + currentRound + '/' + totalRounds;
    }
}

function getTimerText() {
    if (!gameStarted) {
        return getUntilString(gameStartTime);
    } else if (gameEnded) {
        return '0:00';
    } else {
        return getUntilString(currentRoundEnd);
    }
}

function getUntilString(until) {
    let now = new Date().getTime();
    let diff = until - now;
    if (diff > 0) {
        let minutes = Math.floor(diff / (1000 * 60));
        let seconds = Math.floor((diff % (1000 * 60)) / 1000);
        return minutes + ':' + ('0' + seconds).slice(-2);
    } else {
        return '0:00';
    }
}
